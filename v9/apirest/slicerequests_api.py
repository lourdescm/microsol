from flask import Flask, json
from flask_restful import Api, Resource, reqparse
from dateutil.tz import gettz
import datetime as dt


app = Flask(__name__)
api = Api(app)

messages = [
    {
        "message_id": "0", # id of the message - defined by the sender
        "vertical": "-1",  # vertical identifier
        "time_stamp": "-1",  # Timestamp of sender
        "time_received": "-1",  # Timestamp of receiver
        "slices_requirements":  # Array of slice requirements
            [
                {
                    "slice_request_id": "-1",
                    "service_category": "-1",  # FTP, Video, HTML
                    "service_demand": "50",  # File size in Mbs (1, 5, 10....), Video length (Video), HTML
                    "address": "-1",  # specific url to be accessed (optional), ignored
                    "start_time": "-1",  # start time of the slice in secs
                    "duration": "-1",  # duration of the slice in seconds
                    "geographical_scope": {},  # geographical scope of the slice, ignored
                    "supported_messages": {},  # ignored
                    "integrity": dict(throughput="2000", delay="11"),
                    "accessibility": {},  #
                    "retainability": {},  #
                    "addressed": "0"  # 0 -> Not addressed by receiver, 1 --> addressed (resources assigned)
                }
            ]
    }
]




class message(Resource):
    def get(self, name):
        if (name == "all"):
            return messages, 200
        for message in messages:
             if (name == message["message_id"]):
                return message, 200
        return "message not found", 404

    def post(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("vertical")
        parser.add_argument("time_stamp")
        parser.add_argument("slice_request_id")
        parser.add_argument("service_category")
        parser.add_argument("service_demand")
        parser.add_argument("start_time")
        parser.add_argument("duration")
        parser.add_argument("geographical_scope")
        parser.add_argument("supported_messages")
        parser.add_argument("integrity")
        parser.add_argument("accessibility")
        parser.add_argument("retainability")

        args = parser.parse_args()

        for message in messages:
            if (name == message["message_id"]):
                return "Message with message_id {} already exists".format(name), 400

        now = dt.datetime.now(gettz("Europe/London"))


        #data = json.dumps(dict(foo='bar')),
        #content_type = 'application/json',

        #content = request.integrity,
        #print content,

        message = {
            "message_id": name,
            "vertical": args["vertical"],
            "time_stamp": args["time_stamp"],
            "time_received": now.isoformat(),
            "slices_requirements":
                [
                    {
                        "slice_request_id": args["slice_request_id"],
                        "service_category": args["service_category"],
                        "service_demand": args["service_demand"],
                        "start_time": args["start_time"],
                        "duration": args["duration"],
                        "geographical_scope": args["geographical_scope"],
                        "supported_messages": args["supported_messages"],
                        #"integrity":  json.dumps(args["integrity"]),
                        "integrity":  json.loads(args["integrity"]),
                        "accessibility": args["accessibility"],
                        "retainability": args["retainability"]
                    }
                ]
        }



        messages.insert(0, message)
        return message, 201


    def put(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("age")
        parser.add_argument("occupation")
        args = parser.parse_args()

        for message in messages:
            if (name == message["name"]):
                message["age"] = args["age"]
                message["occupation"] = args["occupation"]
                return message, 200

        message = {
            "name": name,
            "age": args["age"],
            "occupation": args["occupation"]
        }
        messages.insert(message)
        return messages, 201

    def delete(self, name):
        global messages
        messages = [message for message in messages if message["message_id"] != name]
        return "{} is deleted.".format(name), 200




api.add_resource(message, "/message/<string:name>")



app.run(host='0.0.0.0')


