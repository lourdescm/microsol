function fill_networks(response) {
    /// A function for getting the distinct networks and filling the network selector for the reservation form
    networks = [];
    for (var i=0; i<response.length; i++) {
	networks.push(response[i].network_id);
	$('#id_network').append($('<option>', {
	    value: response[i].network_id,
	    text : response[i].network_id
	}));
    }
}

function get_terminal_models(response){
    /// A function for getting the distinct terminal models and returning the list
    models = [];
    for (var i=0; i<response.length; i++) {
	if ($.inArray(response[i].model, models) == -1) {
	    models.push(response[i].model);
	}
    }
    return models;
}

function get_terminal_imeis(response){
    /// A function for getting the distinct terminal IMEI numbers and returning the list
    imeis = [];
    for (var i=0; i<response.length; i++) {
	if ($.inArray(response[i].imei, imeis) == -1) {
	    imeis.push(response[i].imei);
	}
    }
    return imeis;
}

function get_terminal_imei_to_model(response) {
    terminals = {};
    for (var i=0; i<response.length; i++) {
	terminals[response[i].imei] = response[i].model;
    }
    return terminals;
}

function get_user_id_to_username(response) {
    users = {};
    for (var i=0; i<response.length; i++) {
	users[response[i].id] = response[i].username;
    }
    return users;
}

function add_terminal_callback(){
    /// A function for adding a form part to request a single terminal
    NUMBER_OF_TERMINAL_FORMS ++;
    var newform = '<table>' +
	'<tr><td>Model</td><td><select id="terminal-form-model-' + NUMBER_OF_TERMINAL_FORMS + '">' +
	'<option value="--" selected="selected">--</option>';
    for (var i=0; i<TERMINAL_MODELS.length; i++) {
	newform += '<option value="' + TERMINAL_MODELS[i] + '">' + TERMINAL_MODELS[i] + '</option>';
    }
    newform += '</select></td></tr>' +
	'<tr><td>IMEI</td><td><select id="terminal-form-imei-' + NUMBER_OF_TERMINAL_FORMS  + '">'+
	'<option value="--" selected="selected">--</option>';
    for (var i=0; i<TERMINAL_IMEIS.length; i++) {
	newform += '<option value="' + TERMINAL_IMEIS[i] + '">' + TERMINAL_IMEIS[i] + '</option>';
    }
    newform += '</select></td></tr>';
    newform += '<tr><td>Status</td><td><div id="terminal-form-status-' + NUMBER_OF_TERMINAL_FORMS +'"></div></td></tr></table>';

    $('#terminal-reservation-area').append($('<div>', {
	id: 'terminal-form-' + NUMBER_OF_TERMINAL_FORMS,
	html: newform
    }));
}

function process_network_reservations(response) {
    /// A function for processing the response of the AJAX call for network reservations
	var currentdate = new Date();
	var datetime = currentdate.getFullYear() + "-" + ("0" + (currentdate.getMonth() + 1)).slice(-2) + "-" +("0" + currentdate.getDate()).slice(-2) + "T"
                + ("0" + currentdate.getHours()).slice(-2) + ":" + ("0" + currentdate.getMinutes()).slice(-2) + ":" + ("0" + currentdate.getSeconds()).slice(-2);
	console.log(datetime);
    for (var i=0; i<response.length; i++) {
    	//solo mostrar los botones de delete cuando el usuario actual es el dueño de la reserva
    	if (Date.parse(response[i].date_end) >= Date.parse(datetime)) {

    		var reservation = '<tr><td>' + response[i].date_start + '</td><td>' +
                    response[i].date_end + '</td><td>' +
                    USER_ID_TO_USERNAME[response[i].user] + '</td><td>' + response[i].state +
					'</td>';

			console.log(ADMIN_USER);
			console.log(typeof ADMIN_USER);

    		if (ADMIN_USER === "True"){

    			reservation = reservation + '<td><input type="button" value="Delete" class="delete" id="' +
					response[i].id + '" onclick="delete_row(this)"/></td>';

    			if (response[i].state == "on hold"){

    				reservation = reservation + '<td><input type="button" value="Accept" class="accept" id="' +
					response[i].id + '" onclick="accept_row(this)"/></td>';
				}

            } else {

    			if (USER_ID_TO_USERNAME[response[i].user] == USER_NAME){

    				reservation = reservation + '<td><input type="button" value="Delete" class="delete" id="' +
					response[i].id + '" onclick="delete_row(this)"/></td>';
				}
			}

			reservation = reservation + '</tr>';

			$("#network-reservations > tbody:last").append(reservation);
        }
    }
    NET_RESERVATIONS = JSON.parse(JSON.stringify(response));
}

function process_terminal_reservations(response) {
    /// A function for processing the response of the AJAX call for terminal reservations
	var currentdate = new Date();
	var datetime = currentdate.getFullYear() + "-" + ("0" + (currentdate.getMonth() + 1)).slice(-2) + "-" +("0" + currentdate.getDate()).slice(-2) + "T"
                + ("0" + currentdate.getHours()).slice(-2) + ":" + ("0" + currentdate.getMinutes()).slice(-2) + ":" + ("0" + currentdate.getSeconds()).slice(-2);
    for (var i=0; i<response.length; i++) {
    	if (Date.parse(response[i].date_end) >= Date.parse(datetime)) {
            var reservation = '<tr><td>' + response[i].terminal + '</td><td>' +
                TERMINAL_IMEI_TO_MODEL[response[i].terminal] + '</td><td>' +
                response[i].date_start + '</td><td>' +
                response[i].date_end + '</td><td>' +
                USER_ID_TO_USERNAME[response[i].user] + '</td><td>' + response[i].state +'</td>';

            if (ADMIN_USER === "True"){

    			reservation = reservation + '<td><input type="button" value="Delete" class="delete" id="' +
					response[i].id + '" onclick="delete_row(this)"/></td>';

    			if (response[i].state == "on hold"){

    				reservation = reservation + '<td><input type="button" value="Accept" class="accept" id="' +
					response[i].id + '" onclick="accept_row(this)"/></td>';
				}

            } else {

    			if (USER_ID_TO_USERNAME[response[i].user] == USER_NAME){

    				reservation = reservation + '<td><input type="button" value="Delete" class="delete" id="' +
					response[i].id + '" onclick="delete_row(this)"/></td>';
				}
			}

			reservation = reservation + '</tr>';

            $("#terminals-reservations > tbody:last").append(reservation);
        }
    }
    TERMINAL_RESERVATIONS = JSON.parse(JSON.stringify(response));
}


function delete_row(row) {
    var i = row.parentNode.parentNode.rowIndex;
    var table = row.parentNode.parentNode.parentNode.parentNode.id;
    var dir;
    if (table == "terminals-reservations"){
		dir = "bookterminals";
	}else{
    	dir = "booknetwork";
	}

	document.getElementById(table).deleteRow(i); //borra la fila de la tabla pero al recagar la pagina vuelve a aparecer (BBDD)
	//Eliminar el registro de la base de datos (Petición a /rest/delete)
	var id = row.id;
	$.ajax({
		url: 'http://127.0.0.1:8000/red/rest/' + dir + '/' + id,
		type: 'DELETE',
		success: function(data) {
			alert("Reservation deleted");
		}
	});
}

function accept_row(row) {
    var i = row.parentNode.parentNode.rowIndex;
    var table = row.parentNode.parentNode.parentNode.parentNode.id;
    var celdas = document.getElementById(table).rows.item(i).cells;
    var dir;
    if (table == "terminals-reservations"){
		dir = "bookterminals";
	}else{
    	dir = "booknetwork";
	}

	/*var network     = "N1";
    var date_start  = celdas.item(0).innerText;
    var date_end    = celdas.item(1).innerText;
    var description = celdas.item(4).innerText;
    var is_free     = true;*/
    var state = "accepted";

	var data = {'state'	: state}

	console.log(JSON.stringify(data));

		var id = row.id;
    	$.ajax({
        	url: 'http://127.0.0.1:8000/red/rest/' + dir + '/' + id + '/',
        	type: 'PATCH',
			contentType: 'application/json',
		 	dataType   : 'json',
		 	data       : JSON.stringify(data),
        	success: function(data) {
            	alert("Reservation accepted");
        	}
    	});
}


function process_terminals(response) {
    /// A function for processing the response of the AJAX call for terminals
    TERMINALS = JSON.parse(JSON.stringify(response));
    TERMINAL_MODELS = get_terminal_models(response);
    TERMINAL_IMEIS = get_terminal_imeis(response);
    TERMINAL_IMEI_TO_MODEL = get_terminal_imei_to_model(response);
}

function process_users(response) {
    /// A function for processing the response of the AJAX call for users
    USER_ID_TO_USERNAME = get_user_id_to_username(response);
}

function process_network(response) {
    fill_networks(response);
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function reserve_network() {
    /// A function to perform a reservation for a network
    var network     = $('#id_network').val();
    var date_start  = $('#id_date_start').val();
    var date_end    = $('#id_date_end').val();
    var description = $('#id_description').val();
    var is_free     = true;
    var state	="state";

    //poner el estado en la view
    //if (ADMIN_USER == true) {
    //	state	= "accepted";
	//}else {
    //	state	= "on hold";
	//}
	//console.log(ADMIN_USER);
	//console.log(state);

    //var dt1 = new Date(date_start);
    
    for (var i=0; i<NET_RESERVATIONS.length; i++){
	if (
	    ((new Date(date_start) >= new Date(NET_RESERVATIONS[i].date_start)) &&
	     (new Date(date_start) <= new Date(NET_RESERVATIONS[i].date_end))) ||
	    ((new Date(date_end)   >= new Date(NET_RESERVATIONS[i].date_start)) &&
	     (new Date(date_end)   <= new Date(NET_RESERVATIONS[i].date_end)))
	) {
	    is_free = false;
	}
    }


    if (is_free) {
	var data = { 'user'               : USER,
		     'network'            : network,
		     'date_start'         : date_start,
		     'date_end'           : date_end,
		     'description'        : description,
			 'state'			  : state
		   }
	
	console.log(JSON.stringify(data));

	$.ajax({ url        : 'http://127.0.0.1:8000/red/rest/booknetwork/',
		 type       : 'POST',
		 contentType: 'application/json',
		 dataType   : 'json',
		 data       : JSON.stringify(data),
		 success    : function () { notify_success('#id_network_reserve_status'); },
		 error      : function () { notify_error('#id_network_reserve_status'); }
	       });
    } else {
	notify_error('#id_network_reserve_status');
    }

    return is_free;
}

function reserve_terminal(N) {
    /// A function to perform a reservation number N for terminal
    var model       = $('#terminal-form-model-' + N).val();
    var imei        = $('#terminal-form-imei-' + N).val();
    var date_start  = $('#id_date_start').val();
    var date_end    = $('#id_date_end').val();
    var description = $('#id_description').val();
    var state	="state";
    var found = false;

    if ((model == '--') && (imei == '--')){ //no se proporciona ni el modelo ni el imei del terminal
		return false;
    } else if (model == '--') { //falta el modelo
		for (var i=0; i<TERMINALS.length; i++) { //busco en la lista de termianles
	    	if (TERMINALS[i].imei == imei) { //cuando encuentro el que coincide con el imei del terminal seleccionado por el usuario
				found = true; // encontrado = true
				model = TERMINALS[i].model;
				for (var j=0; j<TERMINAL_RESERVATIONS.length; j++) { //compruebo en la lista de reservas de terminales
		  	  		if ( (TERMINAL_RESERVATIONS[j].terminal == imei) &&
						((new Date(date_start) >= new Date(TERMINAL_RESERVATIONS[j].date_start)) &&
						(new Date(date_start) <= new Date(TERMINAL_RESERVATIONS[j].date_end))) ||
						((new Date(date_end)   >= new Date(TERMINAL_RESERVATIONS[j].date_start)) &&
						(new Date(date_end)   <= new Date(TERMINAL_RESERVATIONS[j].date_end)))
					) { //si hay colisión
							found = false; //encontrado  = false
							break; //salgo del for
		    		}
				}
	    	}
		}
    } else { //nos dan el modelo
		for (var i=0; i<TERMINALS.length; i++) { //busco en la lista de termianles
	   		 if (TERMINALS[i].model == model) { //cuando encuentro el que coincide con el modelo del terminal seleccionado por el usuario
				 found = true; // encontrado = true
				 imei = TERMINALS[i].imei;
				for (var j=0; j<TERMINAL_RESERVATIONS.length; j++) {
		    		if ( (TERMINAL_RESERVATIONS[j].terminal == TERMINALS[i].imei) && (
						((new Date(date_start) >= new Date(TERMINAL_RESERVATIONS[j].date_start)) &&
			 			(new Date(date_start) <= new Date(TERMINAL_RESERVATIONS[j].date_end))) ||
						((new Date(date_end)   >= new Date(TERMINAL_RESERVATIONS[j].date_start)) &&
			 			(new Date(date_end)   <= new Date(TERMINAL_RESERVATIONS[j].date_end))))
					) { //si hay colisión
							found = false; //encontrado  = false
							break; //salgo del for
					}
				}
	    	}
		}
    }

    if (found) { //si encontrado = true (terminl encontrado y no hay colision)
		var data = { 'user'               : USER,
		     	'terminal'           : imei,
		     	'date_start'         : date_start,
		    	 'date_end'           : date_end,
		     	'description'        : description,
			 	'state'			  : state
		}
		console.log(JSON.stringify(data));
		$.ajax({ url        : 'http://127.0.0.1:8000/red/rest/bookterminals/',
		 	type       : 'POST',
			 contentType: 'application/json',
			 dataType   : 'json',
			 data       : JSON.stringify(data),
			 success    : function () { notify_success('#terminal-form-status-' + N); },
		 	error      : function () { notify_error('#terminal-form-status-' + N); }
		});
    } else {
		notify_error('#terminal-form-status-' + N);
    }

    return found;
}

function notify_success(id){
    /// A function to communicate success for a reservation
    $(id).append('<p style="">OK</p>');
}

function notify_error(id){
    /// A function to communicate failure for a reservation
    $(id).append('<p style="color:red">FAIL</p>');
}

function submit_callback() {
    /// A function to execute the reservations
    reserve_network();

    for (i=1; i<=NUMBER_OF_TERMINAL_FORMS; i++) {
	reserve_terminal(i);
    }

    reservation_complete();
}

function reservation_complete() {
    /// A function to be called when the reservation is completed
    alert("Reservation complete. Read form to see if it has been successful and reload to find your reservations in the tables");
}

