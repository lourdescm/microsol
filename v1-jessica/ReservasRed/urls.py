"""ReservasRed URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
#from ReservasRed.users import login_controller, logout_controller
#from ReservasRed.restringido import acceso_restringido, acceso_libre
#from ReservasRed.index_controller import index
from django.shortcuts import render
from red.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', login_controller),
    url(r'^logout/', logout_controller),
    url(r'^index/', index),
#    url(r'^view_reservations', viewReservations),
#    url(r'^do_reservation', doReservation),
    url(r'^red/', include('red.urls')),
]
