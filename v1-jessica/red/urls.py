from django.conf.urls import url, include
from . import views
from rest_framework import routers, serializers, viewsets, renderers

router = routers.DefaultRouter()
router.register(r'^rest/terminals', views.TerminalViewSet)
router.register(r'^rest/network', views.NetworkViewSet)
router.register(r'^rest/booknetwork', views.BookNetworkViewSet)
router.register(r'^rest/bookterminals', views.BookTerminalViewSet)
router.register(r'^rest/users', views.UserViewSet)

urlpatterns = [
#    url(r'^terminales/reservas', views.reservas_terminales),
#    url(r'^terminales/reservar', views.reservar_terminal),
    url(r'^', include(router.urls)),
]
