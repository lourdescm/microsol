from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from red.models import Terminal, Network, BookTerminal, BookNetwork
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import viewsets, permissions, status
from rest_framework.decorators import api_view

from red.serializers import NetworkSerializer, TerminalSerializer, BookNetworkSerializer, BookTerminalSerializer, UserSerializer
from django.http import Http404
from django.core.mail import EmailMessage
from time import strftime

# Create your views here.
class TerminalViewSet(viewsets.ModelViewSet):
    queryset = Terminal.objects.all()
    serializer_class = TerminalSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class NetworkViewSet(viewsets.ModelViewSet):
    queryset = Network.objects.all()
    serializer_class = NetworkSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class BookTerminalViewSet(viewsets.ModelViewSet):
    queryset = BookTerminal.objects.all()
    serializer_class = BookTerminalSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def create(self, request):
        serializer = BookTerminalSerializer(data=request.data)
        if serializer.is_valid():
            resource_reservations =serializer.validated_data['terminal'].bookings #Coger todas las reservas que hay en la base de datos para una red
            reservations_collisions = resource_reservations.filter(date_end__gt=serializer.validated_data['date_start']) \
                .filter(date_start__lt=serializer.validated_data['date_end'])
            if reservations_collisions.count() > 0: #Hay colison
                print "Hay colision"
                #return render(request, 'reservation_busy.html')
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else: #No hay colision
                print "No hay colision"
                # Solo si el usuarios es administrador se guardara la reserva, sino se envia un correo para solicitarla
                if (serializer.validated_data['user'].is_staff == False):
                    #print "Usuario no es administrador"
                    serializer.validated_data['state'] = "on hold"
                    subject = "New reservation request"
                    startTime = serializer.validated_data['date_start'].strftime('%Y%m%d%H%M')
                    endTime = serializer.validated_data['date_end'].strftime('%Y%m%d%H%M')
                    message = "From: " + startTime + ". To: " \
                              + endTime + ". Terminal imei: " \
                              + serializer.validated_data['terminal'].imei + ". Usuario: " \
                              + serializer.validated_data['user'].username
                    sender = serializer.validated_data['user'].email
                    recipients = ['sender.mml@gmail.com']
                    email = EmailMessage(subject, message, to=recipients, reply_to=[sender])
                    email.send()
                    print "Mensaje enviado"
                else:
                    serializer.validated_data['state'] = "accepted"

                serializer.save()
                #return render(request, 'reservation_successful.html')
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            #return render(request, 'reservation_bad_form.html')
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        def destroy(self, request, *args, **kwargs):
            try:
                instance = self.get_object()
                self.perform_destroy(instance)
                print "Elemento borrado"
            except Http404:
                pass
            return Response(status=status.HTTP_204_NO_CONTENT)

        def partial_update(self, request, pk=None):
            instance = self.get_object()
            serializer = self.serializer_class(instance, data=request.data, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class BookNetworkViewSet(viewsets.ModelViewSet):
    print "hola"
    queryset = BookNetwork.objects.all()
    serializer_class = BookNetworkSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def create(self, request):
        serializer = BookNetworkSerializer(data=request.data)
        print "entra"
        if serializer.is_valid():
            print serializer.validated_data
            print serializer.validated_data['date_start']
            print serializer.validated_data['date_end']
            resource_reservations =serializer.validated_data['network'].bookings #Coger todas las reservas que hay en la base de datos para una red
            reservations_collisions = resource_reservations.filter(date_end__gt=serializer.validated_data['date_start']) \
                .filter(date_start__lt=serializer.validated_data['date_end'])
            if reservations_collisions.count() > 0: #Hay colison
                print "Hay colision"
                #return render(request, 'reservation_busy.html')
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else: #No hay colision
                print "No hay colision"
                #Solo si el usuarios es administrador se guardara la reserva, sino se envia un correo para solicitarla
                if (serializer.validated_data['user'].is_staff == False):
                    #print "Usuario no es administrador"
                    serializer.validated_data['state'] = "on hold"
                    subject = "New reservation request"
                    startTime = serializer.validated_data['date_start'].strftime('%Y%m%d%H%M')
                    endTime = serializer.validated_data['date_end'].strftime('%Y%m%d%H%M')
                    message = "From: " + startTime + ". To: " \
                              + endTime + ". Description: " \
                              + serializer.validated_data['description'] + ". Usuario: " \
                              + serializer.validated_data['user'].username
                    sender = serializer.validated_data['user'].email
                    recipients = ['sender.mml@gmail.com']
                    email = EmailMessage(subject, message, to=recipients, reply_to=[sender])
                    email.send()
                    print "Mensaje enviado"
                else:
                    serializer.validated_data['state']= "accepted"

                #return render(request, 'reservation_successful.html')
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        else:
            #return render(request, 'reservation_bad_form.html')
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
            print "Elemento borrado"
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    def partial_update(self, request, pk=None):
        print "partial_update"
        instance = self.get_object()
        serializer = self.serializer_class(instance, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


@login_required
def index(request):
    return render(request, 'index.html', context={'user':request.user})


def acceso_libre(request):
    return render(request, 'pagina_publica.html')


@login_required
def acceso_restringido(request):
    return render(request, 'pagina_restringida.html')

def login_controller(request):
    if request.method == 'POST' and request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            next_page = request.POST.get('next', '/')
            if next_page:
                return HttpResponseRedirect(next_page)
            else:
                return render(request, 'index.html')
        else:
            return render(request, 'login.html')
    if request.method == 'GET':
        context = {'next': request.GET.get('next', '/index')}
        return render(request, 'login.html', context=context)


def logout_controller(request):
    logout(request)
    return render(request, 'index.html')


# @login_required
# def viewReservations(request):
#     reservations = ResourceRequest.objects.all()
#     return render(request, 'view_reservations.html', context={'reservations':reservations})


@login_required
def doReservation(request):
    if request.method == 'GET':
        return render(request, 'reservation_form.html')
    elif request.method == 'POST':
        formreq = ResourceRequestForm(request.POST)
        if formreq.is_valid():
            formreq.cleaned_data['user'] = request.user.username
            resource_reservations = None
            if formreq.instance.resource_type == 'network':
                resource_reservations = Network.objects.get(pk=formreq.instance.resource_id).bookings
            elif formreq.instance.resource_type == 'terminal':
                resource_reservations = Terminal.objects.get(pk=formreq.instance.resource_id).bookings
            else:
                return render(request, 'reservation_bad_form.html')
                
            reservations_collisions = resource_reservations.filter(elements=formreq.instance.elements)\
                                                           .filter(date_end__gt=formreq.instance.date_start)\
                                                           .filter(date_start__lt=formreq.instance.date_end)
            if reservations_collisions.count() > 0:
                return render(request, 'reservation_busy.html')
            else:
                if formreq.instance.resource_type == 'network':
                    pass
                elif formreq.instance.resource_type == 'terminal':
                    pass
                return render(request, 'reservation_successful.html')
        else:
            return render(request, 'reservation_bad_form.html')
    else:
        return render(request, 'request_bad_request.html')


# @login_required
# def reservas_terminales(request):
#     terminal_type = ContentType.objects.get_for_model(Terminal)
#     reservas = Book.objects.filter(content_type=terminal_type, date_end__gte=timezone.now()).order_by('date_start')
#     return render(request, 'red/listar_reservas_terminal.html', {'reservas': reservas})


# @login_required
# def reservar_terminal(request):
#     url_template = 'red/reservar_terminal.html'
#     if request.method == 'GET':
#         form_book = BookForm()
#         return render(request, url_template, context={'form_book': form_book})
#     elif request.method == 'POST':
#         form_book = BookForm(request.POST)
#         if form_book.is_valid():
#             form_book.instance.user = request.user
#             form_book.instance.content_type = ContentType.objects.get_for_model(Terminal)
#             booking_collisions = Book.objects.filter(object_id=form_book.instance.object_id,
#                                                      date_start__lte=form_book.instance.date_end,
#                                                      date_end__gte=form_book.instance.date_start)
#             if booking_collisions.count() > 0:
#                 return render(request, url_template,
#                               context={'form_book': form_book, 'msg': 'Terminal is busy'})
#             else:
#                 form_book.save()
#                 return render(request, url_template,
#                               context={'form_book': form_book, 'msg': 'Booking successfully'})
#         else:
#             return render(request, url_template)

        
