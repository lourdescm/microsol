from django.contrib.auth.models import User
from red.models import BookNetwork, BookTerminal, Network, Terminal
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields=('id','username')

class NetworkSerializer(serializers.ModelSerializer):
    #def __init__(self, *args, **kwargs):
    #    many = kwargs.pop('many', True)
    #    super(NetworkSerializer, self).__init__(many=many, *args, **kwargs)
    class Meta:
        model=Network
        depth=1
        fields=('bookings', 'network_id')

class TerminalSerializer(serializers.ModelSerializer):
    #def __init__(self, *args, **kwargs):
    #    many = kwargs.pop('many', True)
    #    super(TerminalSerializer, self).__init__(many=many, *args, **kwargs)
    class Meta:
        model=Terminal
        fields=('model', 'imei')


class BookTerminalSerializer(serializers.ModelSerializer):
    #def __init__(self, *args, **kwargs):
    #    many = kwargs.pop('many', True)
    #    super(BookTerminalSerializer, self).__init__(many=many, *args, **kwargs)
#    user = UserSerializer()
#    terminal = TerminalSerializer()
    class Meta:
        model=BookTerminal
        #depth=1
        fields=('id', 'user', 'date_start', 'date_end', 'terminal', 'state')

    def update(self, instance, validated_data):
        demo = BookTerminal.objects.get(pk=instance.id)
        BookTerminal.objects.filter(pk=instance.id) \
            .update(**validated_data)
        return demo


        
class BookNetworkSerializer(serializers.ModelSerializer):
    #def __init__(self, *args, **kwargs):
    #    many = kwargs.pop('many', True)
    #    super(BookNetworkSerializer, self).__init__(many=many, *args, **kwargs)
 #   user = UserSerializer()
    
    class Meta:
        model=BookNetwork
        fields=('__all__')

    def update(self, instance, validated_data):
        demo = BookNetwork.objects.get(pk=instance.id)
        BookNetwork.objects.filter(pk=instance.id) \
            .update(**validated_data)
        return demo
