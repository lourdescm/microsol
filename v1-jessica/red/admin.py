from django.contrib import admin
#from django.contrib.contenttypes.admin import GenericTabularInline
from .models import Terminal, Network, BookTerminal, BookNetwork

# Register your models here.

class BookNetworkInline(admin.TabularInline):
    model = BookNetwork

class BookTerminalInline(admin.TabularInline):
    model = BookTerminal
    
class NetworkAdmin(admin.ModelAdmin):
    inlines = [
        BookNetworkInline,
        ]

class TerminalAdmin(admin.ModelAdmin):
    inlines = [
        BookTerminalInline,
        ]

#admin.site.register(ResourceRequest)
admin.site.register(Terminal, TerminalAdmin)
admin.site.register(Network, NetworkAdmin)
#admin.site.register(BookAdmin)
