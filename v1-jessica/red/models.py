# -*- coding: iso-8859-15 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


# Create your models here.


class ResourceRequest(models.Model):
    user = models.ForeignKey('auth.User')
    resource_type = models.CharField(max_length=120)
    resource_id = models.IntegerField()
    date_start = models.DateTimeField(help_text='Fecha y hora de inicio')
    date_end = models.DateTimeField(help_text='Fecha y hora de fin')
    task_description = models.CharField(help_text='Descripción de la tarea', max_length=1024)
    #configuration_parameters = models.CharField(help_text='Parámetros de configuración modificados', max_length=1024)


class ResourceRequestForm(forms.ModelForm):
    class Meta:
        model = ResourceRequest
        fields = '__all__'


class Book(models.Model):
    user = models.ForeignKey('auth.User')
    #limit = models.Q(app_label='red', model='terminal') | \
    #    models.Q(app_label='red', model='network')
    #content_type = models.ForeignKey(ContentType,
    #                                 limit_choices_to=limit,
    #                                 null=True,
    #                                 blank=True)
    #object_id = models.PositiveIntegerField()
    #content_object = GenericForeignKey('content_type', 'object_id')
    date_start = models.DateTimeField(blank=False)
    date_end = models.DateTimeField(blank=False)
    description = models.TextField()
    state = models.TextField()
    class Meta:
        abstract = True


        
#class BookForm(forms.ModelForm):
#    class Meta:
#        model = Book
#        fields = ['object_id', 'date_start', 'date_end', 'description']


class Network(models.Model):
    network_id = models.CharField(max_length=15, primary_key=True)
    #books = GenericRelation(Book)


class Terminal(models.Model):
    imei = models.CharField(max_length=15, primary_key=True)
    sim = models.CharField(max_length=20)
    model = models.CharField(max_length=50)
    #books = GenericRelation(Book)

class BookNetwork(Book):
    network = models.ForeignKey(Network, related_name='bookings')

class BookTerminal(Book):
    terminal = models.ForeignKey(Terminal, related_name='bookings')
