package com.example.microsol;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Sesion sesion_nav=(Sesion)getApplication();
        sesion_nav.setSesionVariable("");
        setContentView(R.layout.activity_home);


        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {




            switch(menuItem.getItemId()){

               case R.id.navigation_home:

                   Intent i= new Intent(MainActivity.this, Home.class);


                   startActivity(i);
                    break;
                case R.id.navigation_reservas:
                    Intent c= new Intent(MainActivity.this, Reservas.class);


                    startActivity(c);

                    break;
                case R.id.navigation_cuenta:
                    Intent d= new Intent(MainActivity.this, Cuenta.class);


                    startActivity(d);

                    break;

            }


            return true;
        }
    };
}
