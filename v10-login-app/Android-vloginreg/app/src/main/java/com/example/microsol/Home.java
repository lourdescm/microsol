package com.example.microsol;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.content.Intent;

public class Home extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    final Sesion sesion_nav=(Sesion)getApplication();

                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Home.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Home.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:
                            if(sesion_nav.getSesionVariable()!=""){

                                Intent d= new Intent(Home.this, Usuario.class);


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Home.this, Cuenta.class);

                                d.putExtra("username",sesion_nav.getSesionVariable());
                                startActivity(d);
                            }

                            break;

                    }


                    return true;
                }
            };
}
