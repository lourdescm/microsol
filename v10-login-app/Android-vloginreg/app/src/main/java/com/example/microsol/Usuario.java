package com.example.microsol;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Usuario extends AppCompatActivity {

    TextView tvUsuario,tvId;
    Button btn_logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        tvUsuario=(TextView)findViewById(R.id.campo_usuario);
        Intent intent=getIntent();
        String username=intent.getStringExtra("username");

        btn_logout=(Button)findViewById(R.id.boton_logout);

        tvUsuario.setText(username);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Sesion VariableSesion=(Sesion)getApplication();
                VariableSesion.setSesionVariable("");
                Intent intentReg= new Intent(Usuario.this,Cuenta.class);
                Usuario.this.startActivity(intentReg);

            }
        });








    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    final Sesion sesion_nav=(Sesion)getApplication();

                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Usuario.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Usuario.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:
                            if(sesion_nav.getSesionVariable()!=""){

                                Intent d= new Intent(Usuario.this, Usuario.class);


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Usuario.this, Cuenta.class);

                                d.putExtra("username",sesion_nav.getSesionVariable());
                                startActivity(d);
                            }

                            break;

                    }


                    return true;
                }
            };
}
