package com.example.microsol;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Calendar;

public class Reservas extends Activity implements View.OnClickListener {

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";
    private String fecha_entrada="";
    private String fecha_salida="";

    private static final String BARRA = "-";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    //Hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    //Widgets
    EditText etFecha_entrada, etHora_entrada,etFecha_salida, etHora_salida;
    ImageButton bt_entrada,bt_salida;
    Button boton_res;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Sesion sesion_nav=(Sesion)getApplication();


        setContentView(R.layout.activity_reservas);
        if(sesion_nav.getSesionUsuario()!=""){



            setContentView(R.layout.activity_reservas);

            BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
            bottomNav.setOnNavigationItemSelectedListener(navListener);

            etFecha_entrada = (EditText) findViewById(R.id.fecha_entrada);
            // etHora_entrada = (EditText) findViewById(R.id.hora_entrada);

            etFecha_salida = (EditText) findViewById(R.id.fecha_salida);
            //etHora_salida = (EditText) findViewById(R.id.hora_salida);

            bt_entrada = (ImageButton) findViewById(R.id.boton_entrada);
            bt_salida = (ImageButton) findViewById(R.id.boton_salida);

            boton_res=(Button)findViewById(R.id.boton_reservar);

            bt_entrada.setOnClickListener(this);
            bt_salida.setOnClickListener(this);

            boton_res.setOnClickListener(this);


        }else{


            setContentView(R.layout.activity_nologin);

            BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
            bottomNav.setOnNavigationItemSelectedListener(navListener);

        }



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.boton_entrada:
                obtenerHora_entrada();
                obtenerFecha_entrada();

                break;
            case R.id.boton_salida:
                obtenerHora_salida();
                obtenerFecha_salida();

                break;
            case R.id.boton_reservar:



                final Sesion VariableSesion=(Sesion)getApplication();
                String user=VariableSesion.getSesionUsuario();
                String password=VariableSesion.getSesionPassword();
                String id=VariableSesion.getSesionId();
                Response.Listener<String> responseListener=new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse=new JSONObject(response);

                            boolean success=jsonResponse.getBoolean("success");

                            if(success){


                                /*String username= jsonResponse.getString("username");
                                String session_key= jsonResponse.getString("sesion");


                                VariableSesion.setSesionUsuario(username);
                                VariableSesion.setSesionKey(session_key);
                                Intent intent= new Intent(Cuenta.this, Usuario.class);
                                intent.putExtra("username",username);
                                intent.putExtra("sesion",session_key);
                                // intent.putExtra("id",id);

                                Cuenta.this.startActivity(intent);*/

                                AlertDialog.Builder builder=new AlertDialog.Builder(Reservas.this);
                                builder.setMessage("reserva realizada con exito").setNegativeButton("Retry",null)
                                        .create().show();


                                Intent intent= new Intent(Reservas.this, Reserva_completada.class);

                                Reservas.this.startActivity(intent);


                            }else{
                                String error= jsonResponse.getString("error");
                                AlertDialog.Builder builder=new AlertDialog.Builder(Reservas.this);
                                builder.setMessage(error).setNegativeButton("Retry",null)
                                        .create().show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };
                ReservasRequest reservasRequest= new ReservasRequest(fecha_entrada,fecha_salida,user,password,id,responseListener);
                RequestQueue queue= Volley.newRequestQueue(Reservas.this);
                queue.add(reservasRequest);


                break;
        }




    }

    private void obtenerFecha_entrada(){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                final int mesActual = month + 1;

                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);

                fecha_entrada=year+ BARRA + mesFormateado + BARRA +diaFormateado;
               // etFecha_entrada.setText( fecha_entrada );


            }
        },anio, mes, dia);

        recogerFecha.show();

    }

    private void obtenerHora_entrada(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String horaFormateada =  (hourOfDay < 9)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                String minutoFormateado = (minute < 9)? String.valueOf(CERO + minute):String.valueOf(minute);



                //etHora_entrada.setText(horaFormateada + DOS_PUNTOS + minutoFormateado );

               fecha_entrada=fecha_entrada+" "+horaFormateada + DOS_PUNTOS + minutoFormateado;

                etFecha_entrada.setText(fecha_entrada );
            }

        }, hora, minuto, true);

        recogerHora.show();
    }


    private void obtenerFecha_salida(){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                final int mesActual = month + 1;

                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);

                fecha_salida=year+ BARRA + mesFormateado + BARRA +diaFormateado;



            }
        },anio, mes, dia);

        recogerFecha.show();

    }

    private void obtenerHora_salida(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String horaFormateada =  (hourOfDay < 9)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                String minutoFormateado = (minute < 9)? String.valueOf(CERO + minute):String.valueOf(minute);


                fecha_salida=fecha_salida+" "+horaFormateada + DOS_PUNTOS + minutoFormateado;

                etFecha_salida.setText(fecha_salida );
            }

        }, hora, minuto, true);

        recogerHora.show();
    }



    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    final Sesion sesion_nav=(Sesion)getApplication();



                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Reservas.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Reservas.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:

                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Reservas.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Reservas.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;
                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Reservas.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };

}
