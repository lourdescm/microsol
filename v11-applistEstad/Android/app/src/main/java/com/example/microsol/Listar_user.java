package com.example.microsol;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Listar_user extends AppCompatActivity {

    TextView c1,c2,c3,c4,c5,c6,c7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_user);

        final Sesion VariableSesion = (Sesion) getApplication();
        String user = VariableSesion.getSesionUsuario();
        String password = VariableSesion.getSesionPassword();
        String id = VariableSesion.getSesionId();


        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);



        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    boolean success = jsonResponse.getBoolean("success");


                    if (success) {


                        String nrestotal=jsonResponse.getString("nrestotal");
                        String  conmedio=jsonResponse.getString("conmedio");
                        String  conmaximo=jsonResponse.getString("conmaximo");
                        String conminimo=jsonResponse.getString("conminimo");
                        String  contotal=jsonResponse.getString("contotal");
                        String  ultcargador=jsonResponse.getString("ultcargador");

                        //String[]reservas=new String[lista.length()];
                        Log.i("nres  ",nrestotal);

                        Log.i("contotal ",contotal);
                        c1=(TextView)findViewById(R.id.estd_numrestotal);

                        c1.setText(nrestotal);

                        c2=(TextView)findViewById(R.id.estd_ultres);

                        c2.setText(ultcargador);

                        c3=(TextView)findViewById(R.id.estd_conmed);

                        c3.setText(conmedio);

                        c4=(TextView)findViewById(R.id.estd_conmax);

                        c4.setText(conmaximo);

                        c5=(TextView)findViewById(R.id.estd_conmin);

                        c5.setText(conminimo);

                        c6=(TextView)findViewById(R.id.estd_contotal);

                        c6.setText(contotal);






                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(Listar_user.this);
                        builder.setMessage("No tienes estadísticas de uso!!!").setNegativeButton("Retry", null)
                                .create().show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        ListarUserRequest userRequest = new ListarUserRequest(id, responseListener);
        RequestQueue queue = Volley.newRequestQueue(Listar_user.this);
        queue.add(userRequest);

}



    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    final Sesion sesion_nav=(Sesion)getApplication();



                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Listar_user.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Listar_user.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:

                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Listar_user.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Listar_user.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;

                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Listar_user.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };
}
