package com.example.microsol;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Registro extends AppCompatActivity implements View.OnClickListener {


    EditText edtusuario,edtpassword,edtemail;
    Button btn_registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        edtusuario=(EditText)findViewById(R.id.texto_r_usuario);
        edtpassword=(EditText)findViewById(R.id.texto_r_password);
        edtemail=(EditText)findViewById(R.id.texto_r_email);

        btn_registrar=(Button)findViewById(R.id.boton_registrar);

        btn_registrar.setOnClickListener(this);





    }

    @Override
    public void onClick(View view) {

        final String usuario=edtusuario.getText().toString();
        final String password=edtpassword.getText().toString();
        final String email=edtemail.getText().toString();
        final Sesion VariableSesion=(Sesion)getApplication();
        VariableSesion.setSesionPassword(password);
        Response.Listener<String>respoListener= new  Response.Listener<String>(){


            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonResponse= new JSONObject(response);
                    boolean success= jsonResponse.getBoolean("success");


                    if(success){
                        String username= jsonResponse.getString("username");


                        String session_key= jsonResponse.getString("sesion");
                        String userid =jsonResponse.getString("user_id");


                        VariableSesion.setSesionUsuario(username);
                        VariableSesion.setSesionKey(session_key);
                        VariableSesion.setSesionId(userid);

                        Intent intent_r=new Intent(Registro.this,Usuario.class);

                        intent_r.putExtra("username",username);
                        intent_r.putExtra("sesion",session_key);

                        Registro.this.startActivity(intent_r);
                    }else{

                        String error= jsonResponse.getString("error");
                        AlertDialog.Builder builder=new AlertDialog.Builder(Registro.this);
                        builder.setMessage(error).setNegativeButton("Retry",null)
                                .create().show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        RegisterRequest registerRequest=new RegisterRequest(usuario,password,email, respoListener);
        RequestQueue queue= Volley.newRequestQueue(Registro.this);
        queue.add(registerRequest);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    final Sesion sesion_nav=(Sesion)getApplication();

                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Registro.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Registro.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:
                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Registro.this, Usuario.class);

                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());
                                startActivity(d);
                            }else{

                                Intent d= new Intent(Registro.this, Cuenta.class);

                               // d.putExtra("username",sesion_nav.getSesionUsuario());
                                startActivity(d);
                            }

                            break;
                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Registro.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };


}
