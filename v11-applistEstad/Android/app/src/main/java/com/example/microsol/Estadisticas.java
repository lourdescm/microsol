package com.example.microsol;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Estadisticas extends AppCompatActivity {

    TextView c1,c2,c3,c4,c5,c6,c7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadisticas);

        final Sesion VariableSesion = (Sesion) getApplication();
        String user = VariableSesion.getSesionUsuario();
        String password = VariableSesion.getSesionPassword();
        String id = VariableSesion.getSesionId();


        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);



        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    boolean success = jsonResponse.getBoolean("success");


                    if (success) {


                        String nrestotal=jsonResponse.getString("restotales");
                        String resmes=jsonResponse.getString("resmes");
                        String  mes=jsonResponse.getString("mes");
                        String  conmes=jsonResponse.getString("conmes");
                        String contotal=jsonResponse.getString("contotal");
                        String  etotal=jsonResponse.getString("etotal");
                        String  emes=jsonResponse.getString("emes");

                        //String[]reservas=new String[lista.length()];
                        Log.i("resmes  ",resmes);

                        c1=(TextView)findViewById(R.id.gen_mes);

                        c1.setText(mes);

                        c2=(TextView)findViewById(R.id.gen_restotal);

                        c2.setText(nrestotal);

                        c3=(TextView)findViewById(R.id.gen_resmes);

                        c3.setText(resmes);

                        c4=(TextView)findViewById(R.id.gen_contotal);

                        c4.setText(contotal);

                        c5=(TextView)findViewById(R.id.gen_conmes);

                        c5.setText(conmes);

                        c6=(TextView)findViewById(R.id.gen_etotal);

                        c6.setText(etotal);

                        c7=(TextView)findViewById(R.id.gen_emes);

                        c7.setText(emes);






                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(Estadisticas.this);
                        builder.setMessage("No hay estadisticas de uso en este mes!!!").setNegativeButton("Retry", null)
                                .create().show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        ListarGenRequest userRequest = new ListarGenRequest(user,password, responseListener);
        RequestQueue queue = Volley.newRequestQueue(Estadisticas.this);
        queue.add(userRequest);

    }



    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    final Sesion sesion_nav=(Sesion)getApplication();



                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Estadisticas.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Estadisticas.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:

                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Estadisticas.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Estadisticas.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;

                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Estadisticas.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };
}
