package com.example.microsol;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class ListarGenRequest extends StringRequest {


    private static final String RESERVAS_REQUEST_URL="http://192.168.43.125:8000/app_estadisticasGenerales/";



    private Map<String, String> params;



    public ListarGenRequest(String user,String password, Response.Listener<String>listener ){



        super(Request.Method.POST,RESERVAS_REQUEST_URL,listener, null);


        params=new HashMap<>();


        params.put("username",user);
        params.put("password",password);


    }






    @Override
    public Map<String, String> getParams() {


        return params;
    }
}
