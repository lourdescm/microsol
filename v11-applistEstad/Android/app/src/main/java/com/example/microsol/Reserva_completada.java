package com.example.microsol;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Reserva_completada extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva_completada);




        final Sesion VariableSesion=(Sesion)getApplication();
        String user=VariableSesion.getSesionUsuario();
        String password=VariableSesion.getSesionPassword();
        String id=VariableSesion.getSesionId();

        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        Response.Listener<String> responseListener=new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse=new JSONObject(response);

                    boolean success=jsonResponse.getBoolean("success");



                    if(success){

                        JSONArray lista=jsonResponse.getJSONArray("reservas");
                        //String[]reservas=new String[lista.length()];


                        int tam=lista.length();
                        LinearLayout lin=findViewById(R.id.linear);


                        for(int i=0;i<tam;i++){

                            TextView  tv= new TextView(getApplicationContext());
                            tv.setId(i);
                            tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                            tv.setGravity(Gravity.CENTER_HORIZONTAL);
                            tv.setTextColor(Color.BLACK);
                            tv.setText(lista.getString(i));
                            lin.addView(tv);

                            Log.i("array",lista.getString(i));




                        }






                    }else{

                        AlertDialog.Builder builder=new AlertDialog.Builder(Reserva_completada.this);
                        builder.setMessage("No tienes reservas aún!!!").setNegativeButton("Retry",null)
                                .create().show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        ListarReservasRequest reservasRequest= new ListarReservasRequest(id,responseListener);
        RequestQueue queue= Volley.newRequestQueue(Reserva_completada.this);
        queue.add(reservasRequest);
    }



    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    final Sesion sesion_nav=(Sesion)getApplication();



                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Reserva_completada.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Reserva_completada.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:

                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Reserva_completada.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Reserva_completada.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;
                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Reserva_completada.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };
}
