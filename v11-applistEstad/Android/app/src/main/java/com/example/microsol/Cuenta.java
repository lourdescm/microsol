package com.example.microsol;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/*import com.android.volley.AuthFailureError;
import com.android.volley.Header;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;*/

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;


import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

/*import com.android.volley.Request;


import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;*/

import com.android.volley.Response;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public class Cuenta extends AppCompatActivity {

    TextView tv_registrar;
    Button btn_iniciar;
    EditText edtusuario,edtpassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);

        edtusuario=(EditText)findViewById(R.id.texto_usuario);
        edtpassword=(EditText)findViewById(R.id.texto_password);




        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        btn_iniciar=(Button)findViewById(R.id.boton_iniciar);

        tv_registrar=(TextView)findViewById(R.id.texto_registrar); //asociamos el texto a una vista
        tv_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentReg= new Intent(Cuenta.this,Registro.class);
                Cuenta.this.startActivity(intentReg);
            }
        });



        btn_iniciar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View view) {

                final String username=edtusuario.getText().toString();
                final String password=edtpassword.getText().toString();
                final Sesion VariableSesion=(Sesion)getApplication();
                VariableSesion.setSesionPassword(password);





                Response.Listener<String> responseListener=new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse=new JSONObject(response);

                            boolean success=jsonResponse.getBoolean("success");




                            Log.i("SUCCES", "VALUE OF SUCCESS------------------------:" +success);


                            if(success){


                                String username= jsonResponse.getString("username");
                                String session_key= jsonResponse.getString("sesion");
                                String userid =jsonResponse.getString("user_id");

                                Log.i("USERID","USEERDSKJDKSJ"+userid);



                                VariableSesion.setSesionUsuario(username);
                                VariableSesion.setSesionKey(session_key);
                                VariableSesion.setSesionId(userid);

                                Intent intent= new Intent(Cuenta.this, Usuario.class);
                                intent.putExtra("username",username);
                                intent.putExtra("sesion",session_key);

                                //intent.putExtra("cookies",cookies);
                               // intent.putExtra("id",id);

                                Cuenta.this.startActivity(intent);

                            }else{
                                String error= jsonResponse.getString("error");
                                AlertDialog.Builder builder=new AlertDialog.Builder(Cuenta.this);
                                builder.setMessage(error).setNegativeButton("Retry",null)
                                        .create().show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };
                LoginRequest loginRequest= new LoginRequest(username,password,responseListener);
                RequestQueue queue= Volley.newRequestQueue(Cuenta.this);
                queue.add(loginRequest);

            }
        });




    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    final Sesion sesion_nav=(Sesion)getApplication();



                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Cuenta.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Cuenta.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:

                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Cuenta.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());


                                startActivity(d);
                            }else{

                                Intent d= new Intent(Cuenta.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;

                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Cuenta.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }

                    return true;
                }
            };
}
