package com.example.microsol;

import android.app.Application;

public class Sesion extends Application {


    private String usuario;
    private String session_key;
    private String id;
    private String password;

    public String getSesionUsuario(){

        return  this.usuario;
    }

    public void setSesionUsuario(String  variable){

        this.usuario=variable;
    }

    public String getSesionKey(){

        return  this.session_key;
    }

    public void setSesionKey(String  variable){

        this.session_key=variable;
    }


    public String getSesionId(){

        return  this.id;
    }

    public void setSesionId(String  variable){

        this.id=variable;
    }
    public String getSesionPassword(){

        return  this.password;
    }

    public void setSesionPassword(String  variable){

        this.password=variable;
    }
}
