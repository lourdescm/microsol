package com.example.microsol;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class ListarUserRequest extends StringRequest {


    private static final String RESERVAS_REQUEST_URL="http://192.168.43.125:8000/app_estadisticasUser/";



    private Map<String, String> params;



    public ListarUserRequest(String id, Response.Listener<String>listener ){



        super(Request.Method.POST,RESERVAS_REQUEST_URL,listener, null);


        params=new HashMap<>();


        params.put("user_id",id);


    }






    @Override
    public Map<String, String> getParams() {


        return params;
    }
}
