package com.example.microsol;

import android.content.Intent;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Usuario extends AppCompatActivity {

    TextView tvUsuario,tvSesionKey,tvCookies,tv_verreservas,tv_verestadisticas;
    Button btn_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);
        final Sesion VariableSesion=(Sesion)getApplication();
        BottomNavigationView bottomNav=findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        tvUsuario=(TextView)findViewById(R.id.campo_usuario);


        Intent intent=getIntent();
        String username=intent.getStringExtra("username");

        tvUsuario.setText(username);

        tv_verestadisticas=(TextView)findViewById(R.id.texto_estadisticas);
        tv_verestadisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentReg= new Intent(Usuario.this,Listar_user.class);
                Usuario.this.startActivity(intentReg);

            }
        });

        tv_verreservas=(TextView)findViewById(R.id.texto_reservas); //asociamos el texto a una vista
        tv_verreservas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intentReg= new Intent(Usuario.this,Reserva_completada.class);
                Usuario.this.startActivity(intentReg);
            }
        });
        /*tvSesionKey=(TextView)findViewById(R.id.sesionKey);

        String sesion_key=intent.getStringExtra("sesion");

        tvSesionKey.setText(sesion_key);


        tvCookies=(TextView)findViewById(R.id.cookies);

        String sesion_cookies=intent.getStringExtra("cookies");

        tvCookies.setText(sesion_cookies);*/

        btn_logout=(Button)findViewById(R.id.boton_logout);


       /* String user=VariableSesion.getSesionUsuario();


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        DefaultHttpClient httpclient = new DefaultHttpClient();

        HttpPost httpost = new HttpPost("http://192.168.43.232:8000/app_login/");

        List <NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("username", "jesus"));
        nvps.add(new BasicNameValuePair("password", "1111"));

        Log.i("FALLO","FAALONNN");

        try {
            httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
        } catch (UnsupportedEncodingException e) {

            Log.i("FALLO","FAALONNN");
            e.printStackTrace();
        }

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpost);

        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpEntity entity = response.getEntity();

              Log.i("loginresponse","Login form get: " + response.getStatusLine());
        if (entity != null) {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                Log.i("FALLO","FAALONNN");
                e.printStackTrace();
            }
        }

        Log.i("postcokies","Post logon cookies:");
        //List<MinimalField>head =httpclient.getFields();

        List<Cookie>cookies = httpclient.getCookieStore().getCookies();
        if (cookies.isEmpty()) {
           Log.i("cookkiesnone","None");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                Log.i("cokiesllena","- " + cookies.get(i).toString());
            }
        }
        Header[]headerss=httpost.getHeaders("Set-Cookie");


        if (headerss.length<0) {
            Log.i("headerssnone","None");
        } else {
            Log.i("MECAGO","nohace una mierda");


            for (int i = 0; i < headerss.length; i++) {
                Log.i("headersssllena","hedersss " + headerss[i].toString());

            }
        }


        HttpPost httpostres = new HttpPost("http://192.168.43.232:8000/red/rest/bookcharger/");
        List <NameValuePair> nvpsres = new ArrayList<NameValuePair>();
        nvpsres.add(new BasicNameValuePair("user", "21"));
        nvpsres.add(new BasicNameValuePair("charger", "5"));
        nvpsres.add(new BasicNameValuePair("date_start", "2019-06-30 12:00"));
        nvpsres.add(new BasicNameValuePair("date_end", "2019-06-30 22:00"));
        nvpsres.add(new BasicNameValuePair("description", "sdsdsdsdsdsdsd"));
        nvpsres.add(new BasicNameValuePair("state", "on hold"));

        try {
            httpostres.setEntity(new UrlEncodedFormEntity(nvpsres, HTTP.UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            response = httpclient.execute(httpostres);
        } catch (IOException e) {
            e.printStackTrace();
        }
        entity = response.getEntity();
        Log.i("loginresponse","Login form get: " + response.getStatusLine());

        if (entity != null) {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cookies = httpclient.getCookieStore().getCookies();
        if (cookies.isEmpty()) {
            Log.i("cookkiesnone","None");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                Log.i("cokiesllena","- " + cookies.get(i).toString());

            }
        }*/
        //response = httpclient.execute(httpost);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                VariableSesion.setSesionUsuario("");
                VariableSesion.setSesionKey("");
                VariableSesion.setSesionPassword("");
                VariableSesion.setSesionId("");
                Intent intentReg= new Intent(Usuario.this,Cuenta.class);
                Usuario.this.startActivity(intentReg);

            }
        });








    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener=

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    final Sesion sesion_nav=(Sesion)getApplication();

                    switch(menuItem.getItemId()){

                        case R.id.navigation_home:

                            Intent i= new Intent(Usuario.this, Home.class);


                            startActivity(i);
                            break;
                        case R.id.navigation_reservas:
                            Intent c= new Intent(Usuario.this, Reservas.class);


                            startActivity(c);

                            break;
                        case R.id.navigation_cuenta:
                            if(sesion_nav.getSesionUsuario()!=""){

                                Intent d= new Intent(Usuario.this, Usuario.class);
                                d.putExtra("username",sesion_nav.getSesionUsuario());
                                d.putExtra("sesion",sesion_nav.getSesionKey());

                                startActivity(d);
                            }else{

                                Intent d= new Intent(Usuario.this, Cuenta.class);


                                startActivity(d);
                            }

                            break;

                        case R.id.navigation_estadisticas:

                            Intent f= new Intent(Usuario.this, Estadisticas.class);


                            startActivity(f);

                            break;

                    }


                    return true;
                }
            };
}
