package com.example.microsol;



import java.io.IOException;
import android.provider.SyncStateContract;
import android.util.Base64;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//
//import okhttp3.Call;
//import okhttp3.Callback;
//import okhttp3.Cookie;
//import okhttp3.CookieJar;
//import okhttp3.FormBody;
//import okhttp3.Headers;
//import okhttp3.HttpUrl;
//import okhttp3.Interceptor;
//import okhttp3.MediaType;
//import okhttp3.MultipartBody;
//import okhttp3.OkHttpClient;
//
//import okhttp3.RequestBody;


import static android.provider.Telephony.Carriers.PASSWORD;


public class ReservasRequest extends StringRequest {

    //private static final String LOGIN_REQUEST_URL="http://192.168.43.125/Login.php";

    //private PersistentCookieStore cookieStore;
//    private List<Cookie> cookieList = new ArrayList<>();
//    private String token = "token";




    private static final String RESERVAS_REQUEST_URL="http://192.168.43.125:8000/app_reservas/";
    // private static final String LOGIN_REQUEST_URL="http://192.168.0.16:8000/app_login/";

//    Headers headers = new Headers.Builder()
//            .add("X-CSRF-TOKEN", token)
//            .build();


    private Map<String, String> params;



    public ReservasRequest(String fecha_entrada,String fecha_salida, String user, String password, String id,Response.Listener<String>listener ){



        super(Request.Method.POST,RESERVAS_REQUEST_URL,listener, null);


        params=new HashMap<>();

        params.put("username",user);
       // params.put("charger",5+"");
        params.put("date_start",fecha_entrada);
        params.put("date_end",fecha_salida);
        params.put("user_id",id);
        params.put("password",password);
        //params.put("description","CHULASOOO");
       // params.put("state","on hold");

    }

//    @Override
//    public Map<String, String> getHeaders() {
//        HashMap<String, String> headers = new HashMap<>();
//        headers.put("X-CSRF-TOKEN", token);
//        headers.put("Content-Type", "application/json");
//        return headers;
//    }


    @Override
    public Map<String, String> getParams() {


        return params;
    }
}
